

# DayPlan

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fact** | **Integer** | Фактическое |  [optional]
**plan** | **Integer** | План |  [optional]
**deviation** | **Integer** | Отклонение |  [optional]
**currentDay** | **String** | Дата (формат: yyyy-MM-dd) |  [optional]



