package com.nlmk.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.nlmk.api.model.Operation;
import com.nlmk.api.model.ResourceGroupName;
import com.nlmk.api.model.ResourcePlan;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ResourcesGroup
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-04T17:50:28.605192+07:00[Asia/Novosibirsk]")
public class ResourcesGroup   {
  @JsonProperty("groupName")
  @Valid
  private List<ResourceGroupName> groupName = null;

  @JsonProperty("resources")
  @Valid
  private List<ResourcePlan> resources = null;

  @JsonProperty("operation")
  @Valid
  private List<Operation> operation = null;

  public ResourcesGroup groupName(List<ResourceGroupName> groupName) {
    this.groupName = groupName;
    return this;
  }

  public ResourcesGroup addGroupNameItem(ResourceGroupName groupNameItem) {
    if (this.groupName == null) {
      this.groupName = new ArrayList<>();
    }
    this.groupName.add(groupNameItem);
    return this;
  }

  /**
   * Наименование группы (ЖРС / Топливо /Добавки шихты /Флюсы)
   * @return groupName
  **/
  @ApiModelProperty(value = "Наименование группы (ЖРС / Топливо /Добавки шихты /Флюсы)")
  @Valid
  public List<ResourceGroupName> getGroupName() {
    return groupName;
  }

  public void setGroupName(List<ResourceGroupName> groupName) {
    this.groupName = groupName;
  }

  public ResourcesGroup resources(List<ResourcePlan> resources) {
    this.resources = resources;
    return this;
  }

  public ResourcesGroup addResourcesItem(ResourcePlan resourcesItem) {
    if (this.resources == null) {
      this.resources = new ArrayList<>();
    }
    this.resources.add(resourcesItem);
    return this;
  }

  /**
   * Массив ресурсов (Концентрат ГОКА / РУДА  /Доломит /Кокс Алтай)
   * @return resources
  **/
  @ApiModelProperty(value = "Массив ресурсов (Концентрат ГОКА / РУДА  /Доломит /Кокс Алтай)")
  @Valid
  public List<ResourcePlan> getResources() {
    return resources;
  }

  public void setResources(List<ResourcePlan> resources) {
    this.resources = resources;
  }

  public ResourcesGroup operation(List<Operation> operation) {
    this.operation = operation;
    return this;
  }

  public ResourcesGroup addOperationItem(Operation operationItem) {
    if (this.operation == null) {
      this.operation = new ArrayList<>();
    }
    this.operation.add(operationItem);
    return this;
  }

  /**
   * Операция (Выгрузка/Прибытие)
   * @return operation
  **/
  @ApiModelProperty(value = "Операция (Выгрузка/Прибытие)")
  @Valid
  public List<Operation> getOperation() {
    return operation;
  }

  public void setOperation(List<Operation> operation) {
    this.operation = operation;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ResourcesGroup resourcesGroup = (ResourcesGroup) o;
    return Objects.equals(this.groupName, resourcesGroup.groupName) &&
        Objects.equals(this.resources, resourcesGroup.resources) &&
        Objects.equals(this.operation, resourcesGroup.operation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(groupName, resources, operation);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ResourcesGroup {\n");
    
    sb.append("    groupName: ").append(toIndentedString(groupName)).append("\n");
    sb.append("    resources: ").append(toIndentedString(resources)).append("\n");
    sb.append("    operation: ").append(toIndentedString(operation)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
