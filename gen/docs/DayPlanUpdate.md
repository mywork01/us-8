

# DayPlanUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | **String** | Дата (формат: yyyy-MM-dd) |  [optional]
**plan** | **Integer** | План |  [optional]
**resourceId** | [**UUID**](UUID.md) | UUID ресурса (Концентрат ГОКА / РУДА  /Доломит /Кокс Алтай) |  [optional]
**operation** | [**List&lt;Operation&gt;**](Operation.md) | Операция (Выгрузка/Прибытие) |  [optional]



