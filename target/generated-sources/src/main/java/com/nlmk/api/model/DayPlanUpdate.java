package com.nlmk.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.nlmk.api.model.Operation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DayPlanUpdate
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-04T17:50:28.605192+07:00[Asia/Novosibirsk]")
public class DayPlanUpdate   {
  @JsonProperty("date")
  private String date = null;

  @JsonProperty("plan")
  private Integer plan = null;

  @JsonProperty("resourceId")
  private UUID resourceId = null;

  @JsonProperty("operation")
  @Valid
  private List<Operation> operation = null;

  public DayPlanUpdate date(String date) {
    this.date = date;
    return this;
  }

  /**
   * Дата (формат: yyyy-MM-dd)
   * @return date
  **/
  @ApiModelProperty(value = "Дата (формат: yyyy-MM-dd)")

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public DayPlanUpdate plan(Integer plan) {
    this.plan = plan;
    return this;
  }

  /**
   * План
   * @return plan
  **/
  @ApiModelProperty(value = "План")

  public Integer getPlan() {
    return plan;
  }

  public void setPlan(Integer plan) {
    this.plan = plan;
  }

  public DayPlanUpdate resourceId(UUID resourceId) {
    this.resourceId = resourceId;
    return this;
  }

  /**
   * UUID ресурса (Концентрат ГОКА / РУДА  /Доломит /Кокс Алтай)
   * @return resourceId
  **/
  @ApiModelProperty(value = "UUID ресурса (Концентрат ГОКА / РУДА  /Доломит /Кокс Алтай)")

  @Valid
  public UUID getResourceId() {
    return resourceId;
  }

  public void setResourceId(UUID resourceId) {
    this.resourceId = resourceId;
  }

  public DayPlanUpdate operation(List<Operation> operation) {
    this.operation = operation;
    return this;
  }

  public DayPlanUpdate addOperationItem(Operation operationItem) {
    if (this.operation == null) {
      this.operation = new ArrayList<>();
    }
    this.operation.add(operationItem);
    return this;
  }

  /**
   * Операция (Выгрузка/Прибытие)
   * @return operation
  **/
  @ApiModelProperty(value = "Операция (Выгрузка/Прибытие)")
  @Valid
  public List<Operation> getOperation() {
    return operation;
  }

  public void setOperation(List<Operation> operation) {
    this.operation = operation;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DayPlanUpdate dayPlanUpdate = (DayPlanUpdate) o;
    return Objects.equals(this.date, dayPlanUpdate.date) &&
        Objects.equals(this.plan, dayPlanUpdate.plan) &&
        Objects.equals(this.resourceId, dayPlanUpdate.resourceId) &&
        Objects.equals(this.operation, dayPlanUpdate.operation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(date, plan, resourceId, operation);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DayPlanUpdate {\n");
    
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    plan: ").append(toIndentedString(plan)).append("\n");
    sb.append("    resourceId: ").append(toIndentedString(resourceId)).append("\n");
    sb.append("    operation: ").append(toIndentedString(operation)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
