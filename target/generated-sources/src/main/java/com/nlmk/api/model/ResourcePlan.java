package com.nlmk.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.nlmk.api.model.DayPlan;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ResourcePlan
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-04T17:50:28.605192+07:00[Asia/Novosibirsk]")
public class ResourcePlan   {
  @JsonProperty("items")
  @Valid
  private List<DayPlan> items = null;

  @JsonProperty("resourceName")
  private String resourceName = null;

  @JsonProperty("resourceId")
  private UUID resourceId = null;

  public ResourcePlan items(List<DayPlan> items) {
    this.items = items;
    return this;
  }

  public ResourcePlan addItemsItem(DayPlan itemsItem) {
    if (this.items == null) {
      this.items = new ArrayList<>();
    }
    this.items.add(itemsItem);
    return this;
  }

  /**
   * Список план на месяц
   * @return items
  **/
  @ApiModelProperty(value = "Список план на месяц")
  @Valid
  public List<DayPlan> getItems() {
    return items;
  }

  public void setItems(List<DayPlan> items) {
    this.items = items;
  }

  public ResourcePlan resourceName(String resourceName) {
    this.resourceName = resourceName;
    return this;
  }

  /**
   * Наименование ресурса (Концентрат ГОКА / РУДА  /Доломит /Кокс Алтай)
   * @return resourceName
  **/
  @ApiModelProperty(value = "Наименование ресурса (Концентрат ГОКА / РУДА  /Доломит /Кокс Алтай)")

  public String getResourceName() {
    return resourceName;
  }

  public void setResourceName(String resourceName) {
    this.resourceName = resourceName;
  }

  public ResourcePlan resourceId(UUID resourceId) {
    this.resourceId = resourceId;
    return this;
  }

  /**
   * UUID ресурса (Концентрат ГОКА / РУДА  /Доломит /Кокс Алтай)
   * @return resourceId
  **/
  @ApiModelProperty(value = "UUID ресурса (Концентрат ГОКА / РУДА  /Доломит /Кокс Алтай)")

  @Valid
  public UUID getResourceId() {
    return resourceId;
  }

  public void setResourceId(UUID resourceId) {
    this.resourceId = resourceId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ResourcePlan resourcePlan = (ResourcePlan) o;
    return Objects.equals(this.items, resourcePlan.items) &&
        Objects.equals(this.resourceName, resourcePlan.resourceName) &&
        Objects.equals(this.resourceId, resourcePlan.resourceId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(items, resourceName, resourceId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ResourcePlan {\n");
    
    sb.append("    items: ").append(toIndentedString(items)).append("\n");
    sb.append("    resourceName: ").append(toIndentedString(resourceName)).append("\n");
    sb.append("    resourceId: ").append(toIndentedString(resourceId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
