FROM openjdk:11.0.7-jre
COPY target/app.jar /usr/local/service/
ENTRYPOINT ["java", "-jar", "/usr/local/service/app.jar"]
