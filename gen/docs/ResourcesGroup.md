

# ResourcesGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupName** | [**List&lt;ResourceGroupName&gt;**](ResourceGroupName.md) | Наименование группы (ЖРС / Топливо /Добавки шихты /Флюсы) |  [optional]
**resources** | [**List&lt;ResourcePlan&gt;**](ResourcePlan.md) | Массив ресурсов (Концентрат ГОКА / РУДА  /Доломит /Кокс Алтай) |  [optional]
**operation** | [**List&lt;Operation&gt;**](Operation.md) | Операция (Выгрузка/Прибытие) |  [optional]



