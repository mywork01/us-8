package com.nlmk.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DayPlan
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-04T17:50:28.605192+07:00[Asia/Novosibirsk]")
public class DayPlan   {
  @JsonProperty("fact")
  private Integer fact = null;

  @JsonProperty("plan")
  private Integer plan = null;

  @JsonProperty("deviation")
  private Integer deviation = null;

  @JsonProperty("currentDay")
  private String currentDay = null;

  public DayPlan fact(Integer fact) {
    this.fact = fact;
    return this;
  }

  /**
   * Фактическое
   * @return fact
  **/
  @ApiModelProperty(value = "Фактическое")

  public Integer getFact() {
    return fact;
  }

  public void setFact(Integer fact) {
    this.fact = fact;
  }

  public DayPlan plan(Integer plan) {
    this.plan = plan;
    return this;
  }

  /**
   * План
   * @return plan
  **/
  @ApiModelProperty(value = "План")

  public Integer getPlan() {
    return plan;
  }

  public void setPlan(Integer plan) {
    this.plan = plan;
  }

  public DayPlan deviation(Integer deviation) {
    this.deviation = deviation;
    return this;
  }

  /**
   * Отклонение
   * @return deviation
  **/
  @ApiModelProperty(value = "Отклонение")

  public Integer getDeviation() {
    return deviation;
  }

  public void setDeviation(Integer deviation) {
    this.deviation = deviation;
  }

  public DayPlan currentDay(String currentDay) {
    this.currentDay = currentDay;
    return this;
  }

  /**
   * Дата (формат: yyyy-MM-dd)
   * @return currentDay
  **/
  @ApiModelProperty(value = "Дата (формат: yyyy-MM-dd)")

  public String getCurrentDay() {
    return currentDay;
  }

  public void setCurrentDay(String currentDay) {
    this.currentDay = currentDay;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DayPlan dayPlan = (DayPlan) o;
    return Objects.equals(this.fact, dayPlan.fact) &&
        Objects.equals(this.plan, dayPlan.plan) &&
        Objects.equals(this.deviation, dayPlan.deviation) &&
        Objects.equals(this.currentDay, dayPlan.currentDay);
  }

  @Override
  public int hashCode() {
    return Objects.hash(fact, plan, deviation, currentDay);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DayPlan {\n");
    
    sb.append("    fact: ").append(toIndentedString(fact)).append("\n");
    sb.append("    plan: ").append(toIndentedString(plan)).append("\n");
    sb.append("    deviation: ").append(toIndentedString(deviation)).append("\n");
    sb.append("    currentDay: ").append(toIndentedString(currentDay)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
