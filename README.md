TestUs8 Service
======================================

Пререквизиты
----------------
Перед тем как развернуть приложение, необходимо подготовить `settings.xml` и указать в IntelliJ IDEA путь к нему.  
Шаги выполнения:
 1. Скачайте [settings.xml](https://git.nlmk.com/mes-adp/gitlab-ci/-/blob/master/maven/settings.xml).
 2. Откройте загруженный файл и в тегах `username` и `password` укажите свои доменные НЛМК логин и пароль. 
 3. Поместите загруженный файл в локальный репозиторий maven (по умолчанию путь для Windows пользователей: `C:\Users\<User_Name>\.m2`).
 4. Проверьте, что в IntelliJ IDEA указан данный `settings.xml`. Для этого перейдите в `File | Settings | Build, Execution, Deployment | Build Tools | Maven` настройка`User setting file`.   
    Если указан другой `settings.xml`, измените путь к файлу на необходимый.

Сборка
----------------
Для сборки приложения необходимо в корне проекта выполнить следующую команду:

`mvn clean package`  

Локальный запуск
----------------
По умолчанию приложение использует порт 8080. Если этот порт занят, можно установить любой свободный, задав параметр `server.port` в `application.yml`  
Для локального запуска приложения установите соответствующие значения в `application-local.yml`.  
При этом необходимо запускать приложение с профилем `local`.  
Установить профиль можно с помощью параметра JVM `-Dspring.profiles.active=local`  

Проверка качества написанного кода
----------------
Качество кода можно проверить используя maven-checkstyle плагин, встроенный в проект.  
Для этого запустите терминал и выполните следущую команду:

`mvn checkstyle:check`

Дополнительные функциональные возможности
----------------
Помимо решения прикладных задач, приложение по умолчанию поддерживает:
 * Мониторинг с возможностью отображения метрик в формате Prometheus.  
 * UI для задокументированного REST API, доступный на `/swagger-ui` endpoint.