

# ResourcePlan

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;DayPlan&gt;**](DayPlan.md) | Список план на месяц |  [optional]
**resourceName** | **String** | Наименование ресурса (Концентрат ГОКА / РУДА  /Доломит /Кокс Алтай) |  [optional]
**resourceId** | [**UUID**](UUID.md) | UUID ресурса (Концентрат ГОКА / РУДА  /Доломит /Кокс Алтай) |  [optional]



