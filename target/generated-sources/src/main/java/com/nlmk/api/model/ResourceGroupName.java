package com.nlmk.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets ResourceGroupName
 */
public enum ResourceGroupName {
  GRS("GRS"),
    FLUXES("FLUXES"),
    FUEL("FUEL"),
    BATCH_ADDITIVES("BATCH_ADDITIVES");

  private String value;

  ResourceGroupName(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static ResourceGroupName fromValue(String text) {
    for (ResourceGroupName b : ResourceGroupName.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}
