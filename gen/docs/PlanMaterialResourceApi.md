# PlanMaterialResourceApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**materialResourcePlan**](PlanMaterialResourceApi.md#materialResourcePlan) | **POST** /wagon-plan | Добавление плана на конкретные дни по ресурсу
[**materialResourcePlan1**](PlanMaterialResourceApi.md#materialResourcePlan1) | **GET** /wagon-plan | Получение данных на конкретный месяц
[**materialResourcePlan2**](PlanMaterialResourceApi.md#materialResourcePlan2) | **PUT** /wagon-plan | Обновление плана на конкретные дни по ресурсу


<a name="materialResourcePlan"></a>
# **materialResourcePlan**
> List&lt;DayPlanUpdate&gt; materialResourcePlan(plans)

Добавление плана на конкретные дни по ресурсу

### Example
```java
// Import classes:
import org.openapitools.client.ApiClient;
import org.openapitools.client.ApiException;
import org.openapitools.client.Configuration;
import org.openapitools.client.models.*;
import org.openapitools.client.api.PlanMaterialResourceApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    PlanMaterialResourceApi apiInstance = new PlanMaterialResourceApi(defaultClient);
    List<DayPlanUpdate> plans = Arrays.asList(); // List<DayPlanUpdate> | plans
    try {
      List<DayPlanUpdate> result = apiInstance.materialResourcePlan(plans);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PlanMaterialResourceApi#materialResourcePlan");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plans** | [**List&lt;DayPlanUpdate&gt;**](DayPlanUpdate.md)| plans |

### Return type

[**List&lt;DayPlanUpdate&gt;**](DayPlanUpdate.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |
**401** | Unauthorized |  -  |
**403** | Forbidden |  -  |
**404** | Not Found |  -  |

<a name="materialResourcePlan1"></a>
# **materialResourcePlan1**
> List&lt;ResourcesGroup&gt; materialResourcePlan1(planMonth, resourceGroup, operationTypes)

Получение данных на конкретный месяц

### Example
```java
// Import classes:
import org.openapitools.client.ApiClient;
import org.openapitools.client.ApiException;
import org.openapitools.client.Configuration;
import org.openapitools.client.models.*;
import org.openapitools.client.api.PlanMaterialResourceApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    PlanMaterialResourceApi apiInstance = new PlanMaterialResourceApi(defaultClient);
    LocalDate planMonth = new LocalDate(); // LocalDate | Формат даты (формат: yyyy-MM-00)
    List<String> resourceGroup = Arrays.asList(); // List<String> | Тип ресурса (ЖРС / Топливо /Добавки шихты /Флюсы )
    List<Operation> operationTypes = Arrays.asList(); // List<Operation> | Операция (Выгрузка/Прибытие)
    try {
      List<ResourcesGroup> result = apiInstance.materialResourcePlan1(planMonth, resourceGroup, operationTypes);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PlanMaterialResourceApi#materialResourcePlan1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planMonth** | **LocalDate**| Формат даты (формат: yyyy-MM-00) | [optional]
 **resourceGroup** | [**List&lt;String&gt;**](String.md)| Тип ресурса (ЖРС / Топливо /Добавки шихты /Флюсы ) | [optional]
 **operationTypes** | [**List&lt;Operation&gt;**](Operation.md)| Операция (Выгрузка/Прибытие) | [optional]

### Return type

[**List&lt;ResourcesGroup&gt;**](ResourcesGroup.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |
**401** | Unauthorized |  -  |
**403** | Forbidden |  -  |
**404** | Not Found |  -  |

<a name="materialResourcePlan2"></a>
# **materialResourcePlan2**
> List&lt;DayPlanUpdate&gt; materialResourcePlan2(plans)

Обновление плана на конкретные дни по ресурсу

### Example
```java
// Import classes:
import org.openapitools.client.ApiClient;
import org.openapitools.client.ApiException;
import org.openapitools.client.Configuration;
import org.openapitools.client.models.*;
import org.openapitools.client.api.PlanMaterialResourceApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    PlanMaterialResourceApi apiInstance = new PlanMaterialResourceApi(defaultClient);
    List<DayPlanUpdate> plans = Arrays.asList(); // List<DayPlanUpdate> | plans
    try {
      List<DayPlanUpdate> result = apiInstance.materialResourcePlan2(plans);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PlanMaterialResourceApi#materialResourcePlan2");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plans** | [**List&lt;DayPlanUpdate&gt;**](DayPlanUpdate.md)| plans |

### Return type

[**List&lt;DayPlanUpdate&gt;**](DayPlanUpdate.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |
**401** | Unauthorized |  -  |
**403** | Forbidden |  -  |
**404** | Not Found |  -  |

