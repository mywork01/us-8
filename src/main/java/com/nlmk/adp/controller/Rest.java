package com.nlmk.adp.controller;


import com.nlmk.api.WagonPlanApi;
import com.nlmk.api.model.DayPlanUpdate;
import com.nlmk.api.model.ResourcesGroup;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api")
@Log4j2
public class Rest implements WagonPlanApi {

    @Override
    public ResponseEntity<List<DayPlanUpdate>> materialResourceplan(@Valid List<DayPlanUpdate> body) {
        return null;
    }

    @Override
    public ResponseEntity<List<ResourcesGroup>> materialResourceplan1(@Valid DayPlanUpdate body) {
        return null;
    }

    @Override
    public ResponseEntity<List<DayPlanUpdate>> materialResourceplan2(@Valid List<DayPlanUpdate> body) {
        return null;
    }
}

